from Integral_Image import *
from IPoint import *
class SurfDescriptor:
    import cv2
    import numpy as np
    import time
    import math
    from PIL import Image

    img = Integral_Image()
    ip = IPoint()
    ipts = IPoint()
    pi = math.pi

    def SurfDescriptor(self, ipts, upright,extended, img):
        gauss25 = [[0.02350693969273,0.01849121369071,0.01239503121241,0.00708015417522,0.00344628101733,0.00142945847484,0.00050524879060],
                   [0.02169964028389,0.01706954162243,0.01144205592615,0.00653580605408,0.00318131834134,0.00131955648461,0.00046640341759],
                   [0.01706954162243,0.01342737701584,0.00900063997939,0.00514124713667,0.00250251364222,0.00103799989504,0.00036688592278],
                   [0.01144205592615,0.00900063997939,0.00603330940534,0.00344628101733,0.00167748505986,0.00069579213743,0.00024593098864],
                   [0.00653580605408,0.00514124713667,0.00344628101733,0.00196854695367,0.00095819467066,0.00039744277546,0.00014047800980],
                   [0.00318131834134,0.00250251364222,0.00167748505986,0.00095819467066,0.00046640341759,0.00019345616757,0.00006837798818],
                   [0.00131955648461,0.00103799989504,0.00069579213743,0.00039744277546,0.00019345616757,0.00008024231247,0.00002836202103]]

        DescribeInterestPoints(ipts,upright,extended,img)

    def DescribeInterestPoints(self, ipts,upright,extended,img):
        if(ipts.length == 0):
            return "ipts is empty"

        img =img

        for ip in ipts:

            if(extended): ip.descriptionLength = 128
            else: ip.descriptorLength = 64

            if(upgright): GetOrientation(ip)

            GetDescriptor(ip,upright,extended)
            
    def GetOrientation (self, ip):
        Responses = 109
        idx = 0
        ids = [6,5,4,3,2,1,0,1,2,3,4,5,6]

        X = int(round(ip.x))
        Y = int(round(ip.y))
        S = int(round(ip.scale))

        i,j = -6

        for i in range(6):
            for j in range(6):
                if(i * i + j*j < 36):
                    a1 = ids[i+6]
                    a2 = ids[j+6]

                    gauss = gauss25[a1][a2]
                    resX[idx] = gauss * img.HaarX(Y + j* S, X + i * S, 4* S)
                    resY[idx] = gauss * img.HaarY(Y + j* S, X + i * S, 4* S)
                    Ang[idx] = GetAngle(resX[idx],resY[idx])
                    idx = idx +1
        maxx = 0
        orient = 0

        for ang1 in range(2*pi):
            ang1 = ang1 + 0.15
            ang2 = ( ang1 - 5 * pi /3 if ang1 + pi /3 > 2 *pi else ang1 + pi/3)
            sumX,sumY = 0
            for k in range(Responses):
                if(ang1 < ang2 and ang1 < Ang[k] and Ang[k] < ang2):
                    sumX = sumX + resX[k]
                    sumY = sumY + resY[k]

            if(sumX * sumX + sumY * sumY > maxx):
                maxx = sumX * sumX + sumY * sumY
                orient = GetAngle(sumX,sumY)

        ip.orienation = orient

    def GetDescriptor(self, ip,bUpright,bExtended):
        sample_x = 0
        sample_y = 0
        count = 0
        i=0
        ix=0
        j=0
        jx = 0
        xs=0
        ys=0
        gauss_s1 =0
        gauss_s2 = 0
        rx = 0
        ry = 0
        rrx = 0
        rry = 0
        lens = 0
        cx = -0.5
        cy =0

        X = int(round(ip.x))
        Y = int(round(ip.y))
        S = int(round(ip.scale))

        ip.SetDescriptorLength(64)

        if(bUpright):
            co =1
            si =0
        else:
            co = math.cos(ip.orientation)
            si = math.sin(ip.orientation)

        i = -8
        while(i<12):
            j=-8
            i=i -4

            cx = cx + 1
            cy = cy - 0.5
            while (j < 12):
                cy = cy+1
                j=j - 4
                ix = i+5
                jx = j+5

                dx = dy = mdx = mdy = 0
                dx_yn = mdx_yn = dy_xn = mdy_xn = 0

                xs = int(round(X + (-jx * S * si + ix * S * co)))
                ys = int(round(Y + ( jx * S * co + ix * S * si)))

                dx = dy = mdx = mdy = 0
                dx_yn = mdx_yn = dy_xn = mdy_xn = 0

                k = i
                l=j
                for k in range(i+9):
                    for j in range(j+9):
                        sample_x = int(round(X + (-l * S * si + k * S * co)))
                        sample_y = int(round(Y + (l * S * co + k * S * si)))

                        gauss_s1 = GaussianInt(xs - sample_x, ys - sample_y, 2.5 * S)
                        rx = img.HaarX(sample_y, sample_x, 2 * S)
                        ry = img.HaarY(sample_y, sample_x, 2 * S)

                        rrx = gauss_s1 * (-rx * si + ry * co)
                        rry = gauss_s1 * (rx * co + ry * si)

                        if(bExtended):
                            if(rry >=0):
                                dx = dx +rrx
                                mdx = mdx + abs(rrx)
                            else:
                                dx_yn = dx_yn + rrx
                                mdx_yn = mdx_yn + abs(rrx)

                            if(rrx>=0):
                                dy = dy +rry
                                mdy = mdy + abs(rry)
                            else:
                                dy_xn = dy_xn + rry
                                mdy_xn = mdy_xn + abs(rry)
                        else:
                            dx = dx + rrx
                            dy = dy + rry
                            mdx = mdx + abs(rrx)
                            mdy = mdy + abs(rry)

            gauss_s2 = GaussianNumber(cx - 2, cy - 2, 1.5)
            ip.descriptor[count+1] = dx * gauss_s2
            ip.descriptor[count+1] = dy * gauss_s2
            ip.descriptor[count+1] = mdx * gauss_s2
            ip.descriptor[count+1] = mdy * gauss_s2

            if(bExtended):
                ip.descriptor[count+1] = dx_yn * gauss_s2
                ip.descriptor[count+1] = dy_xn * gauss_s2
                ip.descriptor[count+1] = mdx_yn * gauss_s2
                ip.descriptor[count+1] = mdy_xn * gauss_s2

            lens = lens + (dx * dx + dy * dy + mdx * mdx + mdy * mdy + dx_yn + dy_xn + mdx_yn + mdy_xn) * gauss_s2 * gauss_s2
            j = j+9

        i = i + 9

        if(lens>0):
            for d in range(ip.descriptorLength):
                ip.descriptor[d]/=lens
                    
    def GetAngle(self, X,Y):

        if(X>=0 and Y >=0):
            v = math.atan(Y/X)
        elif(X<0 and Y >=0):
            v = pi - math.atan(-Y/X)
        elif(X < 0 and Y < 0):
            v= pi + math.atan(Y / X)
        elif(X >= 0 and Y < 0):
            v= 2 * pi - math.atan(-Y / X)
        
        return v

    def GaussianInt(self, x,y,sig):
        return (1 / (6.2831 * sig * sig)) * math.exp(-(x * x + y * y) / (2.0 * sig * sig))
    def GaussianNumber(self, x,y,sig):
        return 1 / (6.2831 * sig * sig) * math.exp(-(x * x + y * y) / (2.0* sig * sig))
        
                    
                                    
  
                    
                     
        

    
            
            
    
