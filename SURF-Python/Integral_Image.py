import cv2
import numpy as np
import time
from PIL import Image

class Integral_Image:
    cR = 0.2225
    cG = 0.7169
    cB = 0.0606
    cGR = (cR + cG + cB) / 3

    #Integral Image
    def IntegralImage(image):
        fromtImage(image)

    def integImg(self, image):
        x = np.array(image)
        return x

    def fromImage(self,img):
        pic = self.integImg(img)
        height = pic.shape[0]
        width = pic.shape[1]

        rowsum = 0

        # for x in range(width):
        #     rgb = pic[x,0]
        #     red = rgb >> 16 and 0xFF
        #     green = rgb >> 8 and 0xFF
        #     blue = rgb and 0xFF
        #     rowsum = rowsum + (self.cR * red + self.cG* green + self.cB * blue) /255
        #     pic[0][x]=rowsum
        # for y in range(height):
        #     rowsum = 0
        #     for x in range(width):
        #         rgb = pic[x,0]
        #         red = rgb >> 16 and 0xff
        #         green = rgb >> 8 and 0xff
        #         blue = rgb and 0xff
        #         rowsum = rowsum + (self.cR * red + self.cG* green + self.cB * blue) /255
        #         pic[y][x]= rowsum = pic[y-1][x]
        result = []
        for col in range(height):
            for row in range(width):
                rgb = pic[col, row]
                rowsum += (rgb * self.cGR) if (type(rgb) == type(np.array([1])[0])) else (rgb[0] * self.cR + rgb[1] * self.cG + rgb[2] * self.cB)
            result.append(rowsum)
            rowsum = 0

        return result


    def BoxIntegral(row,col,rows,cols):

        r1 = row-1 if (row<height) else height-1
        c1 = col-1 if (col<width) else width-1
        r2 = row+rows-1 if (row+rows<height) else height-1
        c2 = col+cols-1 if (col+cols<width) else width-1
        A=0
        B=0
        C=0
        D=0

        if (r1 >= 0 and c1 >= 0): A = pic[r1][c1]
        if (r1 >= 0 and c2 >= 0): B = pic[r1][c2]
        if (r2 >= 0 and c1 >= 0): C = pic[r2][c1]
        if (r2 >= 0 and c2 >= 0): D = pic[r2][c2]

        R1 = int(A) - int(B) - int(C) + int(D)
        R2 = 0 if (0>R1) else R1
        return R2

    def HaarX(row,column,size):
        return BoxIntegral(row - size >> 1, column, size, size >> 1)- 1 * BoxIntegral(row - size >> 1, column - size >> 1, size, size >>1)

    def HaarY(row,column,size):
        return BoxIntegral(row, column - size >> 1, size >> 1, size)- 1 * BoxIntegral(row - size >> 1, column - size >> 1, size >> 1, size)

    #End of integral
