from IPoint import *
from ResponseLayer import *
from Integral_Image import *
class FastHessian:
    import cv2
    import numpy as np
    import time
    from PIL import Image
    
    
    #Start of Hessian
    global thresh, octaves, init, ipts,responseMap
    ipts = IPoint()
    responseMap = ResponseLayer()

    def __init__(self, _thresh , _octaves, init_sample, _img):
        
        thresh = _thresh
        octaves = _octaves
        init = init_sample
        img = _img

    def getIpoints(self):
        filter_map = [[0,1,2,3],[1,3,4,5],[3,5,6,7],[7,9,10,11]]

        ipts = []

        #Build the response map
        ti = time.process_time()
        buildResponseMap()
        elapsedTimeRoadMap = time.process_time() - ti
        tii = time.process_time()
    
        b = ResponseLayer()
        m = ResponseLayer()
        t = ResponseLayer()
        
        for o in range(octaves):
            for i in range(i <=1):
                b = responseMap[filter_map[o,i]]
                m = responseMap[filter_map[o,i+1]]
                t = responseMap[filter_map[o,i+2]]

                for r in range(r < t.height):
                    for c in range(c < t.width):
                        if (isExtremum(r,c,t,m,b)):
                            interpolateExtremum(r,c,t,m,b)

        elapsedtimeInterpolate = time.process_time() - tii
        return ipts

    def buildResponseMap(self):
        responseMap = []
    
        w = (img.Width / init)
        h = (img.Height / init)
        s = (init)

        if (octaves >=1):
            responseMap.append(ResponseLayer(w, h, s, 9))
            responseMap.append(ResponseLayer(w, h, s, 15))
            responseMap.append(ResponseLayer(w, h, s, 21))
            responseMap.append(ResponseLayer(w, h, s, 27))
    
        if (octaves >= 2):
            responseMap.append(ResponseLayer(w / 2, h / 2, s * 2, 39))
            responseMap.append(ResponseLayer(w / 2, h / 2, s * 2, 51))

        if (octaves >= 3):
            responseMap.append(ResponseLayer(w / 4, h / 4, s * 4, 75))
            responseMap.append(ResponseLayer(w / 4, h / 4, s * 4, 99))

        if (octaves >= 4):
            responseMap.append(ResponseLayer(w / 8, h / 8, s * 8, 147))
            responseMap.append(ResponseLayer(w / 8, h / 8, s * 8, 195))
	
        if (octaves >= 5):
            responseMap.append(ResponseLayer(w / 16, h / 16, s * 16, 291))
            responseMap.append(ResponseLayer(w / 16, h / 16, s * 16, 387))

        for m in range(m < responseMap.len):
            buildResponseLayer(responseMap[m])

    def buildResponseLayer(self, rL):
        step = rL.step
        border = (rL.filterr - 1) >>1
        lobe = rL.filterr / 3
        filsize = rL.filterr
        inverse_area = 1 / (filsize * filsize)

        r=0
        c=0
        index = 0
        ar = 0
        ac = 0

        for ar in range(ar < rL.height):
            for ac in range(ac < rL.width):
                index = index + 1
                r = ar * step
                c = ac * step
                
                Dxx = img.BoxIntegral(r-lobe+1, c-border,2*lobe-1,w) - img.BoxIntegral(r-lobe+1,c-1/2,2*lobe-1,lobe)*3
                Dyy = img.BoxIntegral(r - b, c - lobe + 1, w, 2 * lobe - 1) - img.BoxIntegral(r - lobe / 2, c - lobe + 1, lobe, 2 * lobe - 1) * 3
                Dxy = + img.BoxIntegral(r - lobe, c + 1, lobe, lobe)+ img.BoxIntegral(r + 1, c - lobe, lobe, lobe)- img.BoxIntegral(r - lobe, c - lobe, lobe, lobe)- img.BoxIntegral(r + 1, c + 1, lobe, lobe)
			

                Dxx *= inverse_area
                Dyy *= inverse_area
                Dxy *= inverse_area	

                rL.responses[index] = Dxx * Dyy - 0.81 * Dxy * Dxy
                rL.laplacian[index] = 1 if (Dxx + Dyy >= 0) else 0

    def isExtremum (self, r, c, t, m, b,):
        layerBorder = (t.filterr + 1) / (2 * t.step)
        if(r <= layerBorder or  r >= t.height - layerBorder or c <= layerBorder or c >= t.width - layerBorder):
            return (bool(false))

        candidate = m.getResponsesR ( r, c, t)
        if(candidate < thresh):
            return(bool(false))

        rr = -1
        cc = -1
        for rr in range(rr <= 1):
            for cc in range(cc <= 1):
                if(t.getResponse(r + rr, c + cc) >= candidate or ((rr != 0 or cc != 0) and m.getResponsesR(r + rr, c + cc, t) >= candidate) or b.getResponsesR(r + rr, c + cc, t) >= candidate):
                    return(bool(false))

        return(bool(true))
                   
    def interpolateExtremum (self, r, c, t, m, b,):
        v1= buildDerivative(r,c,t,m,b)
        v2= buildHessian(r,c,t,m,b)

        H = v2.copy()
        Hi = np.linalg.inv(H)

        aa = Hi[0][0]
        bb = Hi[0][1]
        cc = Hi[0][2]
        dd = Hi[1][0]
        ee = Hi[1][1]
        ff = Hi[1][2]
        gg = Hi[2][0]
        hh = Hi[2][1]
        ii = Hi[2][2]

        a1 =-(v1[0]*aa + v1[1]*bb + v1[2]*cc)
        b1 =-(v1[0]*dd + v1[1]*ee + v1[2]*ff)
        c1 =-(v1[0]*gg + v1[1]*hh + v1[2]*ii)

        filterStep = (m.filterr - b.filterr)
        if(abs(a1) < 0.5 and abs(b1) < 0.5 and abs(c1) < 0.5):
            ipt = Ipoint()
            ipt.x = (c+a1) * t.step
            ipt.y = (r+b1) * t.step

            ipt.scale = 0.1333 * (m.filterr + c1 * filterStep)
            ipt.laplacian = (int)(m.getLaplacianR(r,c,t))
            ipts.append(ipt)
            x1 =(c+a1)*t.step
            y1 =(r+b1)*t.step
            z1 = ipt.scale

    def buildDerivative(self, r , c, t, m, b):
        dx = 0
        dy = 0
        ds = 0

        dx = (m.getResponsesR(r, c + 1, t) - m.getResponsesR(r, c - 1, t))*0.5
        dy = (m.getResponsesR(r + 1, c, t) - m.getResponsesR(r - 1, c, t))*0.5
        ds = (t.getResponse(r, c) - b.getResponsesR(r, c, t)) *0.5
        
        D = []*3
        D[0] = dx
        D[1] = dy
        D[2] = ds
        return D

    def buildHessian(self, r,c,t,m,b):
        v = m.getResponsesR(r,c,t)

        dxx = m.getResponsesR(r, c + 1, t) + m.getResponsesR(r, c - 1, t) - 2 * v
        dyy = m.getResponsesR(r + 1, c, t) + m.getResponsesR(r - 1, c, t) - 2 * v
        dss = t.getResponse(r, c) + b.getResponsesR(r, c, t) - 2 * v
        dxy = (m.getResponsesR(r + 1, c + 1, t) - m.getResponsesR(r + 1, c - 1, t) - m.getResponsesR(r - 1, c + 1, t) + m.getResponsesR(r - 1, c - 1, t)) *.25
        dxs = (t.getResponse(r, c + 1) - t.getResponse(r, c - 1) - b.getResponsesR(r, c + 1, t) + b.getResponsesR(r, c - 1, t)) *.25
        dys = (t.getResponse(r + 1, c) - t.getResponse(r - 1, c) - b.getResponsesR(r + 1, c, t) + b.getResponsesR(r - 1, c, t)) *.25

        H[0][0] = dxx
        H[0][1] = dxy
        H[0][2] = dxs
        H[1][0] = dxy
        H[1][1] = dyy
        H[1][2] = dys
        H[2][0] = dxs
        H[2][1] = dys
        H[2][2] = dss

        return H
        
