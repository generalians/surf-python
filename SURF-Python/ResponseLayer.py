class ResponseLayer:
    import cv2
    import numpy as np
    import time
    from PIL import Image

    #ResponseLayer

    def ResponseLayer(_width, _height, _step, _filter):
        global width
        width = _width
        global height
        height = _height
        global step
        step = _step
        global filterr
        filterr = _filter
        global laplacian
        laplacian = []*(width*height)
        global responses
        responses = []*(width*height)

    def getLaplacian(row, col):
        return laplacian[row * width + column]

    def getLaplacianR(row, col, src):
        scale = width / src.width
        return laplacian[(scale*row)*width + (scale*col)]

    def getResponse(row,col):
        return responses[row*width+col]

    def getResponsesR(row,col,src):
        scale = width / src.width
        return responses[(scale*row)*width + (scale*col)]
        
    
        
